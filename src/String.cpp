/**
    String Functions Implementation for C++
    String.cpp

    @author [yun]
    @version 0.2
    @date 23/06/2014

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/

#include "../include/String.hpp"
#include <cctype>
#include <sstream>
#include <typeinfo>

string String::toLowerCase(const string &s)
{
    string str="";
    for(unsigned int i=0; i<s.length(); i++) {
        str += tolower(s[i]);
    }
    return str;
}

string String::toUpperCase(const string &s)
{
    string str="";
    for(unsigned int i=0; i<s.length(); i++) {
        str += toupper(s[i]);
    }
    return str;
}

template <typename NumberType>
NumberType toNumber(const string &s)
{
    NumberType i;
    istringstream convert(s);
    if(convert >> i) {
        string check;
        convert >> check;
        if(check.empty()) {
            return i;
        } else {
            throw invalid_argument( s + " is not a valid "+ typeid(NumberType).name() );
        }
    } else {
        throw invalid_argument( s + " is not a valid " + typeid(NumberType).name() );
    }
}

int String::toInteger(const string &s)
{
    return toNumber<int>(s);
}

float String::toFloat(const string &s)
{
    return toNumber<float>(s);
}

double String::toDouble(const string &s)
{
    return toNumber<double>(s);
}

unsigned int String::toUInteger(const string &s)
{
    return toNumber<unsigned int>(s);
}

long String::toLong(const string &s)
{
    return toNumber<long>(s);
}

long long String::toLLong(const string &s)
{
    return toNumber<long long>(s);
}

unsigned long long String::toULLong(const string &s)
{
    return toNumber<unsigned long long>(s);
}

long double String::toLDouble(const string &s)
{
    return toNumber<long double>(s);
}


bool String::toBoolean(const string &s)
{
    if(toLowerCase(s)=="true" || toLowerCase(s)=="0" || toLowerCase(s)=="yes") {
        return true;
    } else if(toLowerCase(s)=="false" || toLowerCase(s)=="1" || toLowerCase(s)=="no") {
        return false;
    } else {
        throw invalid_argument( s + " is not a valid boolean value");
    }
}


template <typename NumberType>
string toString(const NumberType &i)
{
    stringstream convert;
    convert << i;
    return convert.str();
}

string String::valueOf(const int &i)
{
    return toString<int>(i);
}

string String::valueOf(const unsigned int &i)
{
    return toString<unsigned int>(i);
}

string String::valueOf(const long &i)
{
    return toString<long>(i);
}

string String::valueOf(const long long &i)
{
    return toString<long long>(i);
}

string String::valueOf(const unsigned long long &i)
{
    return toString<unsigned long long>(i);
}

string String::valueOf(const float &i)
{
    return toString<float>(i);
}

string String::valueOf(const double &i)
{
    return toString<double>(i);
}

string String::valueOf(const long double &i)
{
    return toString<long double>(i);
}

string String::valueOf(bool b)
{
    return ((b)?"true":"false");
}

string String::trim(const string &s, const char *c)
{
    string str=trimLeading(s, c);
    return trimTrailing(str, c);
}

string String::trimWhiteSpace(const string &s, TrimMode mode)
{
    switch(mode) {
    case LEADING:
        return trimLeading(s, " \t\n\r\v\f");
    case TRAILING:
        return trimTrailing(s, " \t\n\r\v\f");
    default:
        string trim = trimLeading(s, " \t\n\r\v\f");
        return trimTrailing(trim, " \t\n\r\v\f");
    }
}

string String::trimLeading(const string &s, const char *c)
{
    string::size_type start = s.find_first_not_of(c);
    return s.substr(start);
}

string String::trimTrailing(const string &s, const char *c)
{
    string::size_type end = s.find_last_not_of(c);
    return s.substr(0, end+1);
}

vector<string> String::split(const string &s, const string &c)
{
    vector<string> tokens;

    if(s.length() < 1) {
        return tokens;
    }

    string::size_type start = 0;
    string::size_type found = s.find(c);

    while(found!=string::npos) {
        tokens.push_back(s.substr(start, found-start));
        start = found+c.length();
        found = s.find(c, found+1);
    }

    if(start != found) {
        tokens.push_back(s.substr(start, found));
    }

    return tokens;
}

string String::join(const vector<string> &str, const string &joiner)
{
    string result;
    vector<string>::const_iterator it=str.begin();

    do {
        result += *it;
        if(++it != str.end()) {
            result += joiner;
        }
    } while(it!=str.end());

    return result;
}

vector<string> String::splitCSV(const string& line)
{
    vector<string> result;

    if(line.length()<1)
        return result;

    bool quot = false;
    string token="";
    int pos=0;

    for(int i=0; i<line.length(); i++){
        if(!quot && line[i]=='\"'){
            quot=true;
            continue;
        }
        if(quot && i==line.length()-1){
            throw invalid_argument("Invalid CSV format.\n[Invalid Data: "+line+"]");
        }
        if(quot){
            if(line[i]=='\"'){
                if(line[i+1]=='\"'){
                    token += line[i++];
                } else if(line[i+1]==' '){
                    throw invalid_argument("Invalid CSV format.\n[Invalid Data: "+line+"]");
                } else {
                    quot=false;
                    result.push_back(token);
                    token="";
                    i++;
                }
                continue;
            }
        } else {
            if(i==line.length()-1 || line[i]==','){
                result.push_back(token);
                token="";
                continue;
            }
        }
        token += line[i];
    }

    return result;
}
