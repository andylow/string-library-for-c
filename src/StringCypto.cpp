/**
    String Cryptographic Functions Implementation for C++
    StringCypto.cpp

    @author [yun]
    @version 0.1
    @date 04/11/2014

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    License to copy and use this software is granted provided that it
    is identified as the "RSA Data Security, Inc. MD5 Message-Digest
    Algorithm" and "Rosetta Code, programming chrestomathy site" in all
    material mentioning or referencing this software
    or this function.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/

#include "../include/StringCypto.h"
#include <cstring>
#include <cstdio>
#include <cstdlib>

typedef unsigned (*MD5Function)(unsigned a[]);

unsigned F( unsigned abcd[] ){
    return ( abcd[1] & abcd[2]) | (~abcd[1] & abcd[3]);
}

unsigned G( unsigned abcd[] ){
    return ( abcd[3] & abcd[1]) | (~abcd[3] & abcd[2]);
}

unsigned H( unsigned abcd[] ){
    return  abcd[1] ^ abcd[2] ^ abcd[3];
}

unsigned I( unsigned abcd[] ){
    return abcd[2] ^ (abcd[1] |~ abcd[3]);
}

unsigned rotate_left( unsigned x, short n ){
    return (((x) << (n)) | ((x) >> (32-(n))));
}

unsigned *md5_process(const char* input, unsigned int length)
{
    //Note: All variables are unsigned 32 bit and wrap modulo 2^32 when calculating

    MD5Function md5f[] = { &F, &G, &H, &I }; // MD5 Functions F, G, H, I
    short M[] = { 1, 5, 3, 7 };              // 1st loop iterator for calculate g
    short O[] = { 0, 1, 5, 0 };              // 2nd loop iterator for calculate g
    // Per-round shift amounts
    short sAmt1[] = { 7,12,17,22};
    short sAmt2[] = { 5, 9,14,20};
    short sAmt3[] = { 4,11,16,23};
    short sAmt4[] = { 6,10,15,21};
    short *sAmt[] = {sAmt1, sAmt2, sAmt3, sAmt4 };;

    // Initialize magic hash number
    static unsigned magic[] = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476 };

    // Use binary integer part of the sines of integers (Radians) as constants:
    // for i from 0 to 63
    //    K[i] := floor(abs(sin(i + 1)) � (2 pow 32))
    // end for
    static unsigned k[64] = { 0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
                              0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
                              0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
                              0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
                              0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
                              0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
                              0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
                              0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
                              0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
                              0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
                              0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
                              0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
                              0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
                              0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
                              0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
                              0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 };

    /** Calculate number of chunks of 512-bit blocks (sixty-four 8-bit words) **/
    int chunks = 1 + (length+8)/64; // Total chunks needed

    /** Pad message length to divisible by 512-bit **/
    int totalSize = 64*chunks;      // Length after padded
    unsigned char *data = (unsigned char*) malloc(totalSize);
    memcpy(data, input, length);
    data[length] = 0x80; // A single bit, 1, is set to the end of the message

    int p = length+1;
    for(; p < totalSize; p++){
        data[p] = 0; // Zeroes are set to the remaining padded space
    }

    p -= 8;
    unsigned oriSize = 8*length; // Length of the original message
    memcpy(data+p, &oriSize, 4 ); // Filled up with 64 bits representing the length of the original message

    /** Main loop **/
    int chunk, i, j, byte;
    unsigned abcd[4]; // denoted A B C D in an array
    short *sAmtN; // Shift amounts in N
    short m, o, g;
    unsigned f; // f value calculate from MD5 function
    MD5Function F; // MD5 function to be used in loop
    unsigned *K = k; // Pointer to K constants

    // Used to encode input (64-bytes) into output (16-bytes).
    union {
        char     input[64];
        unsigned output[16];
    }encode64to16;

    // Process the message in successive 512-bit chunks
    for(chunk = 0, byte = 0; chunk < chunks; chunk++, byte+=64){

        memcpy(encode64to16.input, data+byte, 64);

        for(i=0;i<4;i++) {
            abcd[i] = magic[i]; // Initialize hash value for this chunk:
        }

        for (i = 0; i<4; i++) {
            F = md5f[i]; // Get the MD5 function F,G,H,I
            sAmtN = sAmt[i]; // Get the shift amounts
            m = M[i];
            o = O[i];
            for (j=0; j<16; j++) {
                g = (m*j + o) % 16; // Calculate g
                f = abcd[1] +       // Calcualte f
                    rotate_left(abcd[0] + F(abcd) + K[j+16*i] + encode64to16.output[g], sAmtN[j%4]);

                abcd[0] = abcd[3];
                abcd[3] = abcd[2];
                abcd[2] = abcd[1];
                abcd[1] = f;
            }
        }

        for (i=0; i<4; i++){
            magic[i] += abcd[i]; // Add this chunk's hash to result so far
        }
    }

    return magic;
}

string md5sum(const char* input, int length)
{
    unsigned *digest = md5_process(input, length);

    union{
        unsigned in;
        unsigned char out[4];
    }hex;

    string md5sum;
    char buf[2];
    int k=0;
    for (int i=0; i<4; i++){
        hex.in = digest[i];
        for(int j=0; j<4; j++){
            sprintf(buf, "%02x", hex.out[j]);
            md5sum+=buf;
        }
    }

    return md5sum;
}

string StringCypto::md5(const string& input){
    return md5sum(input.c_str(), input.length());
}

string StringCypto::md5(const char* input){
    return md5sum(input, strlen(input));
}
