/**
    Simple Generator Functions Implementation for C++
    SimpleGenerator.h

    @author [yun]
    @version 0.1
    @date 19/09/2014

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/

#include "../include/SimpleGenerator.h"
#include <cstdlib>
#include <ctime>
#include <iostream>

string SimpleGenerator::randomPassword( int length, PasswordType type ){

    if( length < 1 ) throw invalid_argument( "Password length cannot less than 1" );

    const string charset =
        "0123456789"
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "!@#$%^&*"
        "()[];:'\"<>/\\?|";

    srand(time(NULL));

    string pass = "";
    for( int i=0; i<length; i++){
        pass += charset[rand() % type];
    }
    return pass;
}
