/**
    String Validation Functions Implementation for C++
    Validator.cpp

    @author Andy Low a.k.a [yun]
    @version initial
    @date 13/03/2011

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/

#include "../include/Validator.hpp"

bool Validator::isNumeric(const string &s)
{
    for(string::const_iterator it=s.begin(); it!=s.end(); it++) {
        if(!isdigit(*it)) {
            return false;
        }
    }
    return true;
}

bool Validator::isDecimal(const string &s)
{
    bool valid = false;
    for(string::const_iterator it=s.begin(); it!=s.end(); it++) {
        if(!valid && *it=='.' && it!=s.begin() && it!=s.end()-1) {
            valid = true;
            continue;
        } else if(valid && *it=='.') {
            return false;
        }
        if(!isdigit(*it)) {
            return false;
        }
    }
    return valid;
}

bool Validator::isAlpha(const string &s)
{
    for(string::const_iterator it=s.begin(); it!=s.end(); it++) {
        if(!isalpha(*it)) {
            return false;
        }
    }
    return true;
}

bool Validator::isAlphaNum(const string &s)
{
    for(string::const_iterator it=s.begin(); it!=s.end(); it++) {
        if(!isalnum(*it)) {
            return false;
        }
    }
    return true;
}

bool Validator::isValidIPv4(const string &s)
{
    int i = 0;
    size_t start = 0, end = s.find_first_of(".");
    string ip[4];
    while (end!=string::npos) {
        if(i>=4) {
            return false;
        }
        ip[i++] = s.substr(start, end-start);
        start = end+1;
        end=s.find_first_of(".",start);
    }
    ip[i] = s.substr(start, end-start);
    if(i != 3) {
        return false;
    }
    while( i > 0 ) {
        if( ip[i].length() ==0 || ip[i].length() > 3 || !isNumeric(ip[i]) ) {
            return false;
        }
        i--;
    }
    return true;
}

bool Validator::isEmail(const string &s)
{
    size_t found = s.find_first_of("@");
    if(found == string::npos) {
        return false;
    }

    size_t found2 = s.find_first_of(".", found);
    if(found2 == string::npos || found2 == s.length()-1 || found2 == found+1) {
        return false;
    }

    return true;
}

bool Validator::isCommanEmail(const string &s)
{
    size_t found = s.find_first_of("!#$%^&*()+-={}[]|\\:;'\"<>,/?~`");
    if(found != string::npos) {
        return false;
    }
    return isEmail(s);
}

bool Validator::startsWith(const string &s, const string &start)
{
    string validate = s.substr(0, start.size());
    return (validate==start);
}

bool Validator::endsWith(const string &s, const string &end)
{
    string validate = s.substr(s.size()-end.size(), end.size());
    return (validate==end);
}
