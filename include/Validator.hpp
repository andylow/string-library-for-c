/**
    String Validation Functions for C++
    Validator.cpp

    @author Andy Low a.k.a [yun]
    @version initial
    @date 13/03/2011

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/
#ifndef VALIDATOR_H
#define VALIDATOR_H

#include <string>

using namespace std;


class Validator
{
public:
    static bool isNumeric(const string &s);

    static bool isDecimal(const string &s);

    static bool isAlpha(const string &s);

    static bool isAlphaNum(const string &s);

    static bool isValidIPv4(const string &s);

    static bool isEmail(const string &s);

    static bool isCommanEmail(const string &s);

    static bool startsWith(const string &s, const string &start);

    static bool endsWith(const string &s, const string &end);
};

#endif // VALIDATOR_H
