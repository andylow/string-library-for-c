/**
    Simple Generator Functions for C++
    SimpleGenerator.h

    @author [yun]
    @version 0.1
    @date 19/09/2014

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/

#ifndef SIMPLEGENERATOR_H
#define SIMPLEGENERATOR_H

#include <stdexcept>

using namespace std;

class SimpleGenerator
{
    public:
        enum PasswordType {DIGIT = 10, ALPHANUMERIC = 62, SYMBOL = 70, COMPLEX = 83};
        static string randomPassword( int length , PasswordType type = SYMBOL);
    protected:
    private:
};

#endif // SIMPLEGENERATOR_H
