/**
    @author Andy Low a.k.a [yun]
    @version 0.1
    @date 19/09/2013

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/
#ifndef STRINGUTIL_H
#define STRINGUTIL_H

#include "String.hpp"
#include "StringCypto.h"
#include "Validator.hpp"
#include "SimpleGenerator.h"

#endif
