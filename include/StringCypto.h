/**
    String Cryptographic Functions for C++
    StringCypto.hpp

    @author [yun]
    @version 0.1
    @date 04/11/2014

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/
#ifndef STRINGCYPTO_H
#define STRINGCYPTO_H

#include <string>

using namespace std;

class StringCypto
{
    public:
        /**
            MD5 message-digest hash function

            @param Input string
            @return md5 hashed value
        */
        static string md5(const string& input);

        static string md5(const char* input);
    protected:
    private:
};

#endif // STRINGCYPTO_H
