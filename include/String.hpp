/**
    String Functions for C++
    String.hpp

    @author [yun]
    @version 0.2
    @date 23/06/2014

    Copyright (C) 2014 Andy Low. All rights reserved.
    No copyright is claimed, and the software is hereby placed in the public domain.
    In case this attempt to disclaim copyright and place the software in the
    public domain is deemed null and void.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted. There's ABSOLUTELY NO WARRANTY, express or implied.

    These notices must be retained in any copies of any part of this
    documentation and/or software.
*/
#ifndef STRING_H
#define STRING_H

#include <stdexcept>
#include <vector>

using namespace std;

class String {

public:
    /**
        Converts all of the characters in the string to lower case.

        @param Input string
        @return A copy of input string converted to lower case
     */
    static string toLowerCase(const string &s);

    /**
        Converts all of the characters in the string to upper case.

        @param Input string
        @return A copy of input string converted to upper case
     */
    static string toUpperCase(const string &s);

    /**
        Convert string to integer value

        @param Input string (contains only numbers)
        @return integer

        @throw invalid_argument
     */
    static int toInteger(const string &s);

    /**
        Convert string to floating point value

        @param Input string (contains only numbers)
        @return float

        @throw invalid_argument
     */
    static float toFloat(const string &s);

    /**
        Convert string to double value

        @param Input string (contains only numbers)
        @return double

        @throw invalid_argument
     */
    static double toDouble(const string &s);

    /**
        Convert string to unsigned integer

        @param Input string (contains only numbers)
        @return unsigned integer

        @throw invalid_argument
     */
    static unsigned int toUInteger(const string &s);

    /**
        Convert string to long value

        @param Input string (contains only numbers)
        @return long

        @throw invalid_argument
     */
    static long toLong(const string &s);

    /**
        Convert string to long long value

        @param Input string (contains only numbers)
        @return long long

        @throw invalid_argument
     */
    static long long toLLong(const string &s);

    /**
        Convert string to unsigned long long value

        @param Input string (contains only numbers)
        @return unsigned long long

        @throw invalid_argument
     */
    static unsigned long long toULLong(const string &s);

    /**
        Convert string to long double

        @param Input string (contains only numbers)
        @return unsigned long double

        @throw invalid_argument
     */
    static long double toLDouble(const string &s);

    /**
        Convert string type to bool type

        @param A string of "true" or "false" or "0" or "1" or "yes" or "no"
        @return Boolean value

        @throw invalid_argument
    */
    static bool toBoolean(const string &s);

    /**
        Convert numeric type to string type

        @param Number
        @return A string with number value
     */
    static string valueOf(const int &i);
    static string valueOf(const long &i);
    static string valueOf(const unsigned int &i);
    static string valueOf(const long long &i);
    static string valueOf(const unsigned long long &i);
    static string valueOf(const float &i);
    static string valueOf(const double &i);
    static string valueOf(const long double &i);

    /**
        Convert boolean type to string

        @param Boolean
        @return A string with value of "true" or "false"
     */
    static string valueOf(bool b);

    enum TrimMode{LEADING, TRAILING, BOTH};
    /**
        Remove white space or specific characters from the begin and
        end of the string

        @param Input string, [character to trim]
        @return A copy of input string, with leading and trailing defined character omitted.
     */
    static string trim(const string &s, const char *c=" ");

    static string trimWhiteSpace(const string &s, TrimMode mode = BOTH);

    /**
        Remove white space or specific characters from the begin and
        end of the string

        @param Input string, [character to trim]
        @return A copy of input string, with leading defined character removed.
     */
    static string trimLeading(const string &s, const char *c=" ");

    /**
        Remove white space or specific characters from the begin and
        end of the string

        @param Input string, [character to trim]
        @return A copy of input string, with trailing defined character removed.
     */
    static string trimTrailing(const string &s, const char *c=" ");

    /**
        Split a string vector of tokens by seperator

        @param Input string, [Seperator = " "]
        @return A vector of splitted string
     */
    static vector<string> split(const string &s, const string &seperator=" ");

    /**
        Join a vector of string with joiner

        @param Vector of string, [Joiner = " "]
        @return Joined string with joiner
     */
    static string join(const vector<string> &str, const string &joiner=" ");

    static vector<string> splitCSV(const string &line);
};

#endif

